const fs = require('promise-fs')
const uuid = require('uuid/v1')
const Database = require('../database/Database')

class UserService {

    constructor() {
        //this.db = (process.env.STAGE == 'dev'? 'database/test':'database/prod')+'/users.json';
        //nacersi related
        /*let config  = {
                accessKeyId: 'AKIARQ3AOHX7XUBEBTHN',  
                secretAccessKey: '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' 
        }*/
        
        //serverless-agent related
        let config  = {

        }
        if(process.env.STAGE != 'staging') {
            config.region = 'localhost',
            config.endpoint = 'http://localhost:8000'
        }

        this.manager = new Database(config);
    }
    async all()
    {   
        let users =  await this.manager.all({table: 'users'})
        return users.Items || [];
        
    }
    async show(id) {
        let user =  await this.manager.show({
            table: 'users',
               
                    id: id
            
          })
   
       return user;

    }
    async store(user) {
        user = Object.assign({id: uuid()}, user)
        let res =  await this.manager.store({
            table: 'users',
            item: user
          })
        
         return res;
    }
    async update(id, user) {
        let updatedUser;

        try {
            updatedUser = await this.manager.update({
                table: 'users',
                id:id,
                updatee:{
                    name: user.name
                }
            });

            } catch(err) {
                updatedUser = false
            }
            return updatedUser;
    }
    async destroy(id) {
        let res =  await this.manager.destroy({
            table:'users',
            id:id
           
          
          })
         
       return typeof res === 'Object' ? res: false;
        
    }


}
module.exports = UserService