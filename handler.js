'use strict';
const UserService = require('./services/user_service')
const UserController = require('./controllers/user_controller')
const _uc = new UserController(new UserService());

module.exports.hello = async (event) => {
  return {
    statusCode: 200,
    body: JSON.stringify({"welcome": "There\'s no F place like Home! the mighty event is down huh",
  "event": event
})
  };
  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};

/*module.exports.index = (event) =>_uc.index(event);
module.exports.store = (event)=>_uc.store(event);
module.exports.destroy = (event)=>_uc.destroy(event);*/
module.exports = _uc;

