const chai = require('chai')
const assert = chai.assert
const UserService = require('../../services/user_service')


describe("User Service unit test", function() {
  var userService;

  before(function() {
    userService = new UserService()

  })

  it("Should return users list", async function () {
      
      let users = await userService.all();
        assert.isArray(users);


  })
  it("Should add new user Successfully", async function() {
    let user = {
      name: "Test User to add"
    }
    let newUser = await userService.store(user);
    assert.equal(newUser.name, user.name)
    let users = await userService.all();
    assert.isTrue(users.map(user => user.id).includes(newUser.id));
  })
  it("Should update user Successfully", async function() {
    let user = {
      name: "initial user"
    }
    let newUser = await userService.store(user);

    let userToUpdate = {
      name: "Test User to update"
    }
 
    let id = newUser.id;
    let updatedUser = await userService.update(id, userToUpdate);

    let foundUser = await userService.show(id);    
    assert.equal(foundUser.name,updatedUser.name);
  })
  it("Should delete user", async function() {

    let user = {
      name: "Test User to delete"
    }
    let insertedUser = await userService.store(user)
    let deletedUser = await userService.destroy(insertedUser.id)
    assert.equal(deletedUser.name, user.name);
    let users = await userService.all();
    assert.isFalse(users.map(user => user.id).includes(insertedUser.id));

  })
it("Should return a user", async function() {
    let user = {
      name: "Test User to find"
    }
    let insertedUser = await userService.store(user)
    let foundUser = await userService.show(insertedUser.id);    
    assert.equal(foundUser.id,insertedUser.id);
    
  })
})