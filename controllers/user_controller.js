const _us = require('../services/user_service');
const Controller = require('./Controller');

class UserController extends Controller
{
    
    constructor(us) {   

        super()
        this.userService = us
        this.index = this.index.bind(this)
        this.store = this.store.bind(this)
        this.update = this.update.bind(this)
        this.show = this.show.bind(this)
        this.destroy = this.destroy.bind(this)
    }
 
     async index(event) {

         let users = await this.userService.all()
         let response = {}
         let statusCode
         if(!users) {
             response.body = "Problem returning users!"
            statusCode = 404
         } else {
             response = users
             statusCode = 200
         }
       return this.json(users, statusCode);
    }

    async show(event) {

        let id = event.pathParameters.id;
        let found = await this.userService.show(id);
       
        let response = {}
        if(!found) {
            response.body = "User was not found!"
            response.status = 404
        } else {
            response.body = found
            response.status = 200
        }
     
        return this.json(response.body,response.status);
    }
    async store(event) {

        let user = await this.userService.store(JSON.parse(event.body))
        let response = {}
        let statusCode
        if(user) {
            response.user = user
            response.status = "Added with Success!"
            statusCode = 200
        } else {
            
            response.status = "Failure on Add!"
            statusCode = 404
        }
        return this.json(response, statusCode)
    }
    async update(event) {
        let id = event.pathParameters.id
        let user = await this.userService.update(id, JSON.parse(event.body))
        let statusCode
        let response = {}
        if(user) {
            response.user = user
            response.status = "Updated with Success!"
           statusCode = 200
        } else {
            
            response.status = "Failure on Update!"
            statusCode = 404
        }
        return this.json(response, statusCode)
    }
    async destroy(event) {

        let user = await this.userService.destroy(event.pathParameters.id)
        let status  = user != false ? "Deleted with Success!" : "User was not found! ";
        let statusCode
        let response  = {}
        response.status = status
        if(user) {
            response.user = user
                statusCode = 200
        } else {
                statusCode = 404  
               }
        return this.json(response, statusCode)
    }
}
module.exports = UserController