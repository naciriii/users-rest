const aws = require('aws-sdk')
const DocumentClient = aws.DynamoDB.DocumentClient

class Database
{

    constructor(config) 
    {
        this.db = new aws.DynamoDB(config)
        this.documentClient = new DocumentClient(config)
     

        const tables = (async ()=> {
          let count = await this.db.listTables({ Limit: 5 }).promise();
          return count;
        })();
        tables.then(data => {
         if(!data.TableNames.length) {
           this.createTable('users')
         }

        })
      
    }
 
    async all(params) {

        return this.documentClient.scan({
            TableName:params.table
        }).promise();

    }
    async show(params) {
      let user =  await this.documentClient.get({
        TableName:params.table,
              Key: {
                  id: params.id }
          
        }).promise()
 
     return user.Item;

  }
  async store(params) {
    let res;
     try {
       await this.documentClient.put({
        TableName:params.table,
          Item: params.item
        }).promise()
        res = params.item;
      } catch(err) {

        return false;
      }
      
       return res;
  }
  async update(params) {
  let res;
  let updateString = Object.keys(params.updatee);
  let updateExpression = 'set';
  let updateExpressionValues = {};
  let updateExpressionNames = {};

  for(let c = 0; c < updateString.length; c++) {
    updateExpression+=' #'+updateString[c]+' = :'+updateString[c];
    updateExpressionValues[':'+updateString[c]] = params.updatee[updateString[c]];
    updateExpressionNames['#'+updateString[c]] = updateString[c];
    if(updateString[c+1] != null) {
      updateExpression+=', ';
    }

  }

  try {
   res =  await this.documentClient.update({
      TableName: params.table,
      Key: {
          id: params.id
      },
      UpdateExpression: updateExpression,
      ExpressionAttributeNames: updateExpressionNames,
      ExpressionAttributeValues: updateExpressionValues,
      ConditionExpression: 'attribute_exists(id)',
      ReturnValues: "ALL_NEW"
  }).promise();
  // console.log(res);

  }catch(err) {
    //console.log(err)
    res = false;

  } 
/*   console.log('updateExpression', updateExpression);
  console.log('updateExpressionValues', updateExpressionValues);
  console.log('updateExpressionNames', updateExpressionNames); */
  return res.Attributes;
     
  }
  async destroy(params) {
    let res;
    try {
      res =  await this.documentClient.delete({
          TableName: params.table,
          Key: {
              id: params.id
          },
          ReturnValues: "ALL_OLD"

        
        }).promise()
      } catch(error) {
     return false;
      }
      return res.Attributes;
      
  }

    async createTable(tableName) {
     
    

        const tableParams = {
          TableName: tableName,
          KeySchema: [
            // The type of of schema.  Must start with a HASH type, with an optional second RANGE.
            {
              // Required HASH type attribute
              AttributeName: 'id',
              KeyType: 'HASH',
            }
          ],
          AttributeDefinitions: [
            // The names and types of all primary and index key attributes only
            {
              AttributeName: 'id',
              AttributeType: 'S', // (S | N | B) for string, number, binary
            }
          ],
          ProvisionedThroughput: { // required provisioned throughput for the table
            ReadCapacityUnits: 1,
            WriteCapacityUnits: 1,
          }
        };
    
        const result = await this.db.createTable(tableParams).promise();
    
        return !!result.$response.error;
      }



}
module.exports = Database;